package assertions

import (
	"encoding/json"
	"fmt"
	"reflect"
)

func ShouldResembleJSON(actual interface{}, expected ...interface{}) string {
	var in interface{}
	var out interface{}

	if err := json.Unmarshal([]byte(actual.(string)), &in); err != nil {
		return fmt.Sprintf("could not unmarshal first parameter: %s", err.Error())
	}
	if err := json.Unmarshal([]byte(expected[0].(string)), &out); err != nil {
		return fmt.Sprintf("could not unmarshal second parameter: %s", err.Error())
	}

	if !reflect.DeepEqual(in, out) {
		return fmt.Sprintf(`"%s" does not equal "%s"`, actual, expected)
	}

	return ""
}
