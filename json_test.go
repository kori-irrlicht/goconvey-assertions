package assertions_test

import (
	"fmt"
	"testing"

	. "gitlab.com/kori-irrlicht/goconvey-assertions"
)

func TestShouldResembleJSON(t *testing.T) {
	testCases := []struct {
		In             string
		Out            string
		ShouldResemble bool
	}{
		{`1`, `1`, true},
		{`[1,2]`, `[1,2]`, true},
		{`[1,2]`, `[2,1]`, false},
		{`{"a": 1}`, `{"a": 1}`, true},
		{`{"a": 1}`, `{"b": 1}`, false},
		{`{"a": 1, "b": 2}`, `{"a": 1, "b": 2}`, true},
		{`{"a": 1, "b": 2}`, `{"b": 2, "a": 1}`, true},
	}

	for _, tc := range testCases {
		t.Run(fmt.Sprintf(`JSON "%s" resembles "%s"`, tc.In, tc.Out), func(t *testing.T) {
			res := ShouldResembleJSON(tc.In, tc.Out)
			if tc.ShouldResemble {
				if res != "" {
					t.Errorf(`got "%s"; want ""`, res)
				}
			} else {
				if res == "" {
					t.Errorf(`got ""; want "%s"`, fmt.Sprintf(`"%s" does not equal "%s"`, tc.In, tc.Out))
				}
			}
		})
	}
}
